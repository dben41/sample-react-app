import axios from 'axios';
import React, {Component} from 'react';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
      this.state = {
        users: [],
        curPage: 1,
        perPage: 6,
        totalPages: 2,
        totalEntries: 12
      };

    this.handleChange = this.handleChange.bind(this);
    this.handlePreviousClick = this.handlePreviousClick.bind(this);
    this.handleNextClick = this.handleNextClick.bind(this);
  }

  handleChange(event) {
    this.setState({perPage: event.target.value, curPage: 1}, () => {
      this.getUsers();
    });
  }

  handlePreviousClick(event) {
    this.setState({curPage: this.state.curPage - 1}, ()=> {
      this.getUsers();
    });
  }

  handleNextClick(event) {
    this.setState({curPage: this.state.curPage + 1}, ()=>{
      this.getUsers();
    });
  }

  async componentDidMount() {
    this.getUsers();
  }

  async getUsers(){
    const {data: {data: users, page: curPage, total_pages:totalPages} } = await axios.get(`https://reqres.in/api/users?page=${this.state.curPage}&per_page=${this.state.perPage}`);
    this.setState({users, curPage, totalPages});
  }

  render(){
    return (
      <div className="App">
        <h2 className="title">{this.state.perPage} Users</h2>
        <div className="userListContainer" style = {{width: 1608}}>
          { <ul className="userList">
              {this.state.users.map(user => 
              <li key={user.id} className="card">
                <div className="userInfo">
                  <span className="id"> 
                    ID: {user.id}
                  </span>
                  <img className="profilePicture" src={user.avatar} alt={user.first_name + "-" + user.last_name}></img>
                  <span className = "name">
                      {user.first_name + " " + user.last_name}
                  </span>
                  <span className = "email">
                    <a href={"mailto:" + user.email}>{user.email}</a> 
                  </span>
                </div>
              </li>)}
          </ul> }
          <div className="pagination">
            <div className="paginationButtons">

            </div>
            <button disabled={this.state.curPage === 1} onClick={this.handlePreviousClick} className="paginationButton">PREVIOUS</button>
            <button disabled={this.state.curPage === this.state.totalPages} onClick={this.handleNextClick} className="paginationButton">NEXT</button>
            <select value={this.state.perPage} onChange={this.handleChange} className="paginationSelect">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
