# Sample React App

Created by Daryl Bennett to showcase React Skills. 
Gets false data from [here](https://reqres.in/api/users?page=2&per_page=6).
Emulates [this app](https://csb-78pc4-2gt0eoe7g.vercel.app/).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### Deployed

Launches the test runner in the interactive watch mode.\
See the live deployed example: [here](https://sample-react-app-livid.vercel.app/).

